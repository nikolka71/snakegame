// Fill out your copyright notice in the Description page of Project Settings.


#include "KillBlock.h"
#include "SnakeBase.h"
#include "Components/BoxComponent.h"
#pragma warning(suppress : 4996)



// Sets default values
AKillBlock::AKillBlock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	class UStaticMesh* WallMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Cube")).Object;

		MyRootComponent = CreateDefaultSubobject<UBoxComponent>("RootModel");
		RootComponent = MyRootComponent;
	class UStaticMeshComponent* WallChank;
	WallChank = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WALL"));
	WallChank->SetStaticMesh(WallMesh);
	WallChank->SetRelativeLocation(FVector(0, 0, 0));
	WallChank->AttachTo(MyRootComponent);




}

// Called when the game starts or when spawned
void AKillBlock::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKillBlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CollideWall();

}

void AKillBlock::CollideWall()
{

	TArray<AActor*>CollectedActors;

	GetOverlappingActors(CollectedActors);

	for (int32 i = 0; i < CollectedActors.Num(); ++i)

		CollectedActors[i]->Destroy(true, true);

}

